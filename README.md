# BDD_Cucumber_Framework_Gherkin_Project

## Overview

This repository contains a BDD based Cucmber automation framework for testing RESTful APIs using Rest Assured. The framework is designed to provide a scalable and maintainable structure for writing API tests.

Created RestAssured BDD Cucumber framework using java programming language encapsulating all our requirements.
Behavior-Driven Development (BDD) is a software development concept that encourages collaboration among developers, QA, and non-technical or business participants by using human readable language 'Gherkin' in a software project. This framework utilizes Cucumber, a tool for running automated tests written in plain language 'Gherkin'

## Configuration

1.to configure all Rest APIs and : 
     > Execute the API=Rest Assured 

     > Extract response = used rest Assured library

     > Parse the response = used jsonpath class which belongs to rest assured library .

     > Validate the response = used testNG assertion
     
2.framework is capable of reusability by creating common methods and centralised configuration files.

   a.Here we have created  repositery package which contains a data_repositery class which contains 
     common data like headername,headervalue,hostname and different resources and another class requestbody 
     which contains all requestbodies at centralised location.

   b.Created common trigger method package which has common utility class contains common utility methods
     such as log directory creation ,evidence file creation and wrting data into excel file.
     Repositery of trigger methods for all API requests.   

3.Created feature files for each API testcase.In feature file defined 'Given' ,'When' ,'Then' methods.

4.Created test description class ,annotated implementing methods by given,when,then annotations

5.used hooks @Before ,@After.

6.Created a common test class in cucumber.Options package.Used tags in this test runner

7.Framework is capable of data driven testing using 'Scenario Outline' and 'Examples' componant in feature file

8.Maven dependencies added : 
      1.junit
      2.cucmber-junit
      3.cucmber-java
      4.rest assured
      5.testNG
      6.ApachePOI-a.poi
                  b.poi-ooxml 
                  c.poi-scratchpad 
                  d.poi-excelant
                  e.poi-examples           
     












