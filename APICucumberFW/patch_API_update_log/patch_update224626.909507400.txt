endpoint is :
https://reqres.in/api/users/2

request body is:
{
    "name": "morpheus",
    "job": "zion resident"
}

response header date is :
Sun, 17 Mar 2024 17:16:27 GMT

response body is :
{
    "name": "morpheus",
    "job": "zion resident",
    "updatedAt": "2024-03-17T17:16:27.347Z"
}

