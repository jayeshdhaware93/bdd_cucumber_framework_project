endpoint is :
https://reqres.in/api/users/2

request body is:
{
    "name": "morpheus",
    "job": "zion resident"
}

response header date is :
Sun, 17 Mar 2024 08:51:09 GMT

response body is :
{
    "name": "morpheus",
    "job": "zion resident",
    "updatedAt": "2024-03-17T08:51:09.385Z"
}

