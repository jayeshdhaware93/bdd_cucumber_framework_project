package common_trigger_methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Repositery.RequestBody;

public class Utility extends RequestBody{
	
	

	public static ArrayList<String> readExcelDataFile(String sheetname, String testcase, String excelfile,
			String extension) throws IOException {
		ArrayList<String> arrayData = new ArrayList<String>();
		
//		get project directory name
		String projectdir = System.getProperty("user.dir");
		
		System.out.println("\n");
		System.out.println("project directory is :" + projectdir);

//	locate data file 
		FileInputStream fis = new FileInputStream(projectdir + "\\data_files\\" + excelfile + extension);

//	open data file
//	fetch the count of sheet
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		int count = wb.getNumberOfSheets();
		System.out.println("count ofsheet is:" + count);

		for (int i = 0; i < count; i++) {
			if (wb.getSheetName(i).equals(sheetname)) {
				System.out.println("desired sheet  found at index " + i + ":" + wb.getSheetName(i));

//				access the row data from sheet

				XSSFSheet datasheet = wb.getSheetAt(i);
				Iterator<Row> rows = datasheet.iterator();
				String testCasefound = "NotFound";

				while (rows.hasNext()) {

					Row datarows = rows.next();

					String tcname = datarows.getCell(0).getStringCellValue();
					System.out.println(tcname);

					if (tcname.equals(testcase)) {
						testCasefound = "True";
						System.out.println("test case" + " " + testcase + " " + "is found");
//						fetch cell data from row

						Iterator<Cell> cellvalues = datarows.cellIterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();

							System.out.print(" " + testdata + " ");

//							make array of data fetched
							arrayData.add(testdata);

						}
						break;

					}

				}
				if (testCasefound == "NotFound") {
					System.out.println(testcase + " " + "testcase is not found in sheet:" + wb.getSheetName(i));

				}
				break;

			} 
			else {
				System.out.println(sheetname + " " + "sheet not found in file excel_data_file.xlsx at index:" + i);
			}
		}
		wb.close();
		return arrayData;
	}

	public static void evidenceFileCreator(File filelocation, String filename, String endpoint, String requestbody,
			String resheader, String responsebody) throws IOException {

		File newTextFile = new File(filelocation + "\\" + filename + ".txt");
		System.out.println("\n");
		
		System.out.println("file create with name :"+" " + newTextFile.getName());

//		write data 
		FileWriter writedata = new FileWriter(newTextFile);

		writedata.write("endpoint is :\n" + endpoint + "\n\n");
		writedata.write("request body is:\n" + requestbody + "\n\n");
		writedata.write("response header date is :\n" + resheader + "\n\n");
		writedata.write("response body is :\n" + responsebody + "\n\n");

//		 close the file
		writedata.close();

	}

	public static File createLogDirectory(String dirname) {
//		 fetch the project directory  file name 
		String projectdir = System.getProperty("user.dir");
		
		
		System.out.println("currant project directory is :" + " " + projectdir);

//		check wheather  directory in varaiable name dirname exist in projectdir name and act accordingly

		File directory = new File(projectdir + "\\" + dirname);

		if (directory.exists()) {
			System.out.println(directory + " " + " already exists");

		} else {
			System.out.println(directory + " " + " does not exist,hence creating it");
			directory.mkdir();
			System.out.println(directory + " " + "created");
		}

		return directory;

	}


	public static String testLogName(String name) {
		LocalTime currantTime = LocalTime.now();
		String currantStringTime =currantTime.toString().replaceAll(":", "");
		String TestLogName = name+currantStringTime;
		return TestLogName;
	}

}
