Feature: trigger the GET API to fetch the  list of users

@Get_API_List_user
Scenario: trigger the APi with valid request parameters
        Given configure get list user endpoint
        When trigger the api to hit the endpoint
        Then validate the status code
        And validate the all response body parameters