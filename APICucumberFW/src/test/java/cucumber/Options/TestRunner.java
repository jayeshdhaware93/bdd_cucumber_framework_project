package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features", glue = {"StepDescriptions" },
		 tags = "@Data_driven_testase or @Delete_testcase or @Get_API_List_user or "
				+ " @Get_API_Single_user or @Patch_API_Testcase or @Post_API_Create_user or"
				+ "  @Post_API_Reg_succ or @Put_API_Testcase")

public class TestRunner {

}
