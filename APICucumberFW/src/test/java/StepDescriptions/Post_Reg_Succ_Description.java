package StepDescriptions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Post_Reg_Succ_Description {
	File dir;
	String endpoint;
	String requestbody;
	Response response;
	int status_code;
	String req_email;
	String req_pasw;
	String res_id;
	String res_token;

	@Given("enter email and password in requestbody")
	public void enter_email_and_password_in_requestbody() throws IOException {
		dir = Utility.createLogDirectory("post_reg_succ_log");
		endpoint = Data_repositery.hostname() + Data_repositery.resource_reg_succ();
		requestbody = RequestBody.req_post_reg_succ("post_TC1");

		response = Api_trigger.post_reg_succ_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				requestbody, endpoint);
		Utility.evidenceFileCreator(dir, Utility.testLogName("post_reg_succ"), endpoint, requestbody,
				response.getHeader("Date"), response.getBody().asPrettyString());
//	    throw new io.cucumber.java.PendingException();

	}

	@When("trigger the API with given payload")
	public void trigger_the_api_with_given_payload() {
		System.out.println(response.getBody().asPrettyString());

//		fetch status code and store i to variable
		status_code = response.getStatusCode();
		System.out.println("status code is :" + status_code);

//			fetch request body parameters 

		System.out.println("------request body parameters--------");
		JsonPath req_jsn = new JsonPath(requestbody);
		req_email = req_jsn.getString("email");
		System.out.println(req_email);
		req_pasw = req_jsn.getString("password");
		System.out.println(req_pasw);

//		fetch response body

		System.out.println("-------response body parameters--------");
		res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		res_token = response.getBody().jsonPath().getString("token");
		System.out.println(res_token);
//	    throw new io.cucumber.java.PendingException();

	}

	@Then("validate ststus code")
	public void validate_ststus_code() {
		Assert.assertEquals(status_code, 200);
//	    throw new io.cucumber.java.PendingException();
	}

	@Then("validate response body parameters ")
	public void validate_response_body_parameters() {
		Assert.assertNotNull(res_id);
		Assert.assertNotNull(res_token);
//	    throw new io.cucumber.java.PendingException();
	}
}
