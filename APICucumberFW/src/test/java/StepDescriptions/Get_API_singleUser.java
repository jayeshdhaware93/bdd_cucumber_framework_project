package StepDescriptions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class Get_API_singleUser {

	Response response;
	String endpoint;
	File dir_name;
	int statuscode;
	String res_id;
	String res_email;
	String res_firstName;
	String res_lastName;
	String res_url;
	String res_text;

	@Given("configure get_single_user endpoint")
	public void configure_endpoint() throws IOException {

		endpoint = Data_repositery.hostname() + Data_repositery.resource_get_single_user();
		dir_name = Utility.createLogDirectory("get_single_user_log");
		response = Api_trigger.get_single_user_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("get_single_user"), endpoint, null,
				response.getHeader("Date"), response.asPrettyString());

	}

	@When("trigger the api to hit get_single_user endpoint")
	public void trigger_the_api_to_hit_the_endpoint() {
		response = Api_trigger.get_single_user_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				endpoint);
		System.out.println(response.asPrettyString());

		statuscode = response.getStatusCode();
		System.out.println("status code is:" + statuscode);

		// get response body parameters
		System.out.println("--------respnse body parameters---------");

		res_id = response.getBody().jsonPath().getString("data.id");
		System.out.println(res_id);

		res_email = response.getBody().jsonPath().getString("data.email");
		System.out.println(res_email);

		res_firstName = response.getBody().jsonPath().getString("data.first_name");
		System.out.println(res_firstName);

		res_lastName = response.getBody().jsonPath().getString("data.last_name");
		System.out.println(res_lastName);

		res_url = response.getBody().jsonPath().getString("support.url");
		System.out.println(res_url);

		res_text = response.getBody().jsonPath().getString("support.text");
		System.out.println(res_text);

	}

	@Then("validate for status code")
	public void validate_for_status_code() {
		Assert.assertEquals(statuscode, 200);

	}

	@Then("validate response body parameters of get api")
	public void validate_reponse_body_parameters_of_get_api() {
		Assert.assertEquals(res_id, "2");
		Assert.assertEquals(res_email, "janet.weaver@reqres.in");
		Assert.assertEquals(res_firstName, "Janet");
		Assert.assertEquals(res_lastName, "Weaver");
		Assert.assertEquals(res_url, "https://reqres.in/#support-heading");
		Assert.assertEquals(res_text, "To keep ReqRes free, contributions towards server costs are appreciated!");
	}
}
