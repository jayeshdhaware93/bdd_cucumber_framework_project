package StepDescriptions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Patch_API_update_ {

	String endpoint;
	String requestbody;
	Response response;
	File dir_name;
	String req_name;
	String req_job;
	String res_name;
	String res_job;
	String res_time;
	String exp_time;
	int StatusCcode;

	@Given("enter valid name and job in requestbody of patch_api")
	public void enter_valid_name_and_job_in_requestbody_of_patch_api() throws IOException {
		endpoint = Data_repositery.hostname() + Data_repositery.resource_patch_update();
		requestbody = RequestBody.req_patch_update();
		dir_name = Utility.createLogDirectory("patch_API_update_log");
		response = Api_trigger.patch_update_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				requestbody, endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("patch_update"), endpoint, requestbody,
				response.getHeader("Date"), response.asPrettyString());
//	    throw new io.cucumber.java.PendingException();
	}

	@When("trigger the api to hit the endpoint of patch_api")
	public void trigger_the_api_to_hit_the_endpoint_of_patch_api() {
		StatusCcode = response.getStatusCode();
		System.out.println("\n" + "status code is:" + " " + StatusCcode);
		System.out.println("\n" + "-----response body is ------");

		System.out.println(response.asPrettyString());
//				create request body object to fetch request body parameters
		JsonPath req_jsn = new JsonPath(requestbody);
		System.out.println("\n" + "-----request body parameters-------");
		req_name = req_jsn.getString("name");
		System.out.println("name:" + req_name);
		req_job = req_jsn.getString("job");
		System.out.println("job:" + req_job);

//				extract response body parameters
		System.out.println("\n" + "-------response body parameters-------");

		res_name = response.getBody().jsonPath().getString("name");
		System.out.println("name:" + res_name);
		res_job = response.getBody().jsonPath().getString("job");
		System.out.println("job:" + res_job);
		res_time = response.getBody().jsonPath().getString("updatedAt");
		System.out.println("updated at:" + res_time);
		res_time = res_time.substring(0, 11);
		System.out.println(res_time);

//				get local time
		LocalDateTime curranttime = LocalDateTime.now();
		exp_time = curranttime.toString().substring(0, 11);
//	    throw new io.cucumber.java.PendingException();
	}

	@Then("validate status code for patch_api")
	public void validate_status_code_for_patch_api() {
		Assert.assertEquals(StatusCcode, 200);
//	    throw new io.cucumber.java.PendingException();
	}

	@Then("validate response body parameters of patch_api")
	public void validate_response_body_parameters_of_patch_api() {
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_time, exp_time);
//	    throw new io.cucumber.java.PendingException();
	}
}
