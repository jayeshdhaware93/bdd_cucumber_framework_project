package StepDescriptions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Data_driven_post_API {
	String requestbody;
	String endpoint;
	Response response;
	File dir_name;
	int status_code;
	String req_name;
	String req_job;
	String res_name;
	String res_job;
	String res_id;
	String res_createdat;
	String exp_time;
	
	

	@Given("enter {string} and {string} in request body")
	public void enter_and_in_request_body(String req_name, String req_job) throws IOException {
		dir_name = Utility.createLogDirectory("postApi_create_log");

		requestbody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job + "\"\r\n"
				+ "}";
		endpoint = Data_repositery.hostname() + Data_repositery.resource_post_create();
		response = Api_trigger.post_trigger(Data_repositery.headername(), Data_repositery.headervalue(), requestbody,
				endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("post_create"), endpoint, requestbody,
				response.getHeader("date"), response.getBody().asPrettyString());

	}

	@When("trigger the api with given request data")
	public void trigger_the_api_with_given_request_data() {
		status_code = response.getStatusCode();

		System.out.println("---------expected parameters------------");
		JsonPath req_jsn = new JsonPath(requestbody);
		req_name = req_jsn.getString("name");
		System.out.println(req_name);
		req_job = req_jsn.getString("job");
		System.out.println(req_job);

//	   fetch response body parameters
		System.out.println("------------response body parameters-------------");
		res_name = response.getBody().jsonPath().getString("name");
		System.out.println(res_name);
		res_job = response.getBody().jsonPath().getString("job");
		System.out.println(res_job);
		res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		res_createdat = response.getBody().jsonPath().getString("createdAt");
		System.out.println(res_createdat);
		res_createdat = res_createdat.substring(0, 11);
		System.out.println(res_createdat);

//	    get local time 
		LocalDateTime curranttime = LocalDateTime.now();
		exp_time = curranttime.toString().substring(0, 11);

	}

	@Then("validate status code of post api data driven")
	public void validate_status_code_of_post_api_data_driven() {
		Assert.assertEquals(status_code, 201);
	}

	@Then("validate responsebody parameters for data driven")
	public void validate_responsebody_parameters_for_data_driven() {
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotEquals(res_id, "0");
		Assert.assertEquals(res_createdat, exp_time);
	}

}
