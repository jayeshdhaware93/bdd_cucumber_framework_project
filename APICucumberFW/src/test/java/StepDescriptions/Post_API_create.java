package StepDescriptions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Post_API_create {

	String requestbody;
	String endpoint;
	Response response;
	File dir_name;
	int status_code1;
	String req_name;
	String req_job;
	String res_name;
	String res_job;
	String res_id;
	String res_createdat;
	String exp_time;

	
	@Before
	public void pre_setup() {
		System.out.println(" \n\n"+"open the browser and launch the url" + "\n");
		
	}
	
	@After
	public void teardown() {
		System.out.println("\n"+"end the testcase and close the browser"+"\n\n");
	}
	
	@Given("enter name and job in request body")
	public void enter_name_and_job_in_request_body() throws IOException {
		dir_name = Utility.createLogDirectory("postApi_create_log");
		requestbody = RequestBody.req_post_create("post_TC2");
		endpoint = Data_repositery.hostname() + Data_repositery.resource_post_create();
		response = Api_trigger.post_trigger(Data_repositery.headername(), Data_repositery.headervalue(), requestbody,
				endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("post_create"), endpoint, requestbody,
				response.getHeader("date"), response.getBody().asPrettyString());

//	    throw new io.cucumber.java.PendingException();
	}

	@When("trigger the api with given payload")
	public void trigger_the_api_with_given_payload() throws IOException {

		status_code1 = response.getStatusCode();

		System.out.println("---------expected parameters------------");
		JsonPath req_jsn = new JsonPath(requestbody);
		req_name = req_jsn.getString(RequestBody.key_innameORemail);
		System.out.println(req_name);
		req_job = req_jsn.getString(RequestBody.key_injobORpasw);
		System.out.println(req_job);

//		   fetch response body parameters
		System.out.println("------------response body parameters-------------");
		res_name = response.getBody().jsonPath().getString(RequestBody.key_innameORemail);
		System.out.println(res_name);
		res_job = response.getBody().jsonPath().getString(RequestBody.key_injobORpasw);
		System.out.println(res_job);
		res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		res_createdat = response.getBody().jsonPath().getString("createdAt");
		System.out.println(res_createdat);
		res_createdat = res_createdat.substring(0, 11);
		System.out.println(res_createdat);

//		    get local time 
		LocalDateTime curranttime = LocalDateTime.now();
		exp_time = curranttime.toString().substring(0, 11);

//		throw new io.cucumber.java.PendingException();
	}

	@Then("validate status code")
	public void validate_status_code() {
		Assert.assertEquals(status_code1, 201);
//		throw new io.cucumber.java.PendingException();
	}

	@Then("validate response body parameters")
	public void validate_response_body_parameters() {

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotEquals(res_id, "0");
		Assert.assertEquals(res_createdat, exp_time);
//		throw new io.cucumber.java.PendingException();

	}

}
