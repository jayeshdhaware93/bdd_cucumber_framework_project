package StepDescriptions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;

public class Delete_API {

	String endpoint;
	File dir_name;
	Response response;
	int Stauscode;

	@Given("configure the endpoint of delete_api")
	public void configure_the_endpoint_of_delete_api() throws IOException {
		endpoint = Data_repositery.hostname() + Data_repositery.resource_delete();
		dir_name = Utility.createLogDirectory("delete_API_log");
		response = Api_trigger.delete_trigger(Data_repositery.headername(), Data_repositery.headervalue(), endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("delete_API"), endpoint, null,
				response.getHeader("Date"), null);
//	    throw new io.cucumber.java.PendingException();
	}

	@When("trigger the api to hit the endpoint of delete_api")
	public void trigger_the_api_to_hit_the_endpoint_of_delete_api() {
		Stauscode = response.getStatusCode();

		System.out.println("status code is :" + Stauscode);
//	    throw new io.cucumber.java.PendingException();
	}

	@Then("validate status code for delete_api")
	public void validate_status_code_for_delete_api() {
		Assert.assertEquals(Stauscode, 204);
//	    throw new io.cucumber.java.PendingException();
	}

}
