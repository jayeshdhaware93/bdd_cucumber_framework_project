package StepDescriptions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Repositery.Data_repositery;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Get_API_ListUser {
	String endpoint;
	File dir_name;
	Response response;
	int status_code3;
	int count;
	String res_body;
	int ids[] = { 7, 8, 9, 10, 11, 12 };
	String Emails[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
			"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
	String firstNames[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
	String lastNames[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };

	int res_id;
	String res_email;
	String res_firstName;
	String res_lastName;

	int i;

	@Given("configure get list user endpoint")
	public void configure_endpoint() throws IOException {
		endpoint = Data_repositery.hostname() + Data_repositery.resource_get_user_list();
		dir_name = Utility.createLogDirectory("get_API_user_list_log");
		response = Api_trigger.get_list_user_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				endpoint);
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("get_list_user"), endpoint, null,
				response.getHeader("Date"), response.getBody().asPrettyString());

//	    throw new io.cucumber.java.PendingException();
	}

	@When("trigger the api to hit the endpoint")
	public void trigger_the_api_to_hit_the_endpoint() {
		// fetch response body and status code
		res_body = response.getBody().asPrettyString();
		System.out.println(response.getBody().asPrettyString());
		status_code3 = response.getStatusCode();
		System.out.println("stsus code is:" + " " + status_code3);

		// fetch response body parameters
//			create json object of response body to fetch length of data array

		JsonPath res_jsn = new JsonPath(res_body);
		count = res_jsn.getInt("data.size()");
		System.out.println("count of data array from response:" + " " + count);

		int idArr[] = new int[count];
		String emailArr[] = new String[count];
		String fnamesArr[] = new String[count];
		String lnamesArr[] = new String[count];

		// fetch data and store into newly created array
		for (int i = 0; i < count; i++) {

			System.out.println("\n" + "---------fetched array data from arrays of expected data ---------");

			int res_id = res_jsn.getInt("data[" + i + "].id");
			System.out.println("\n" + res_id);
			idArr[i] = res_id;

			String res_email = res_jsn.get("data[" + i + "].email");
			System.out.println(res_email);
			emailArr[i] = res_email;

			String res_firstName = res_jsn.getString("data[" + i + "].first_name");
			System.out.println(res_firstName);
			fnamesArr[i] = res_firstName;

			String res_lastName = res_jsn.getString("data[" + i + "].last_name");
			System.out.println(res_lastName);
			lnamesArr[i] = res_lastName;
		}

	}

//	    throw new io.cucumber.java.PendingException();
	@Then("validate the status code")
	public void validate_the_status_code() {
		Assert.assertEquals(status_code3, 200);
	}

	@Then("validate the all response body parameters")
	public void validate_responsebody_parameters() {
		JsonPath res_jsn = new JsonPath(res_body);
		int idArr[] = new int[count];
		String emailArr[] = new String[count];
		String fnamesArr[] = new String[count];
		String lnamesArr[] = new String[count];
//					create arrays to store fetched data from declared array	

		for (int i = 0; i < count; i++) {

			int res_id = res_jsn.getInt("data[" + i + "].id");
			idArr[i] = res_id;

			res_email = res_jsn.getString("data[" + i + "].email");
			emailArr[i] = res_email;

			res_firstName = res_jsn.getString("data[" + i + "].first_name");
			fnamesArr[i] = res_firstName;

			res_lastName = res_jsn.getString("data[" + i + "].last_name");
			lnamesArr[i] = res_lastName;

			// validate response body parameters

			Assert.assertEquals(ids[i], idArr[i]);
			Assert.assertEquals(Emails[i], emailArr[i]);
			Assert.assertEquals(firstNames[i], fnamesArr[i]);
			Assert.assertEquals(lastNames[i], lnamesArr[i]);

		}
	}
}