endpoint is :
https://reqres.in/api/users/2

request body is:
{
    "email": "pekka@yahoo.com",
    "password": "pekka07"
}

response header date is :
Sun, 17 Mar 2024 17:19:00 GMT

response body is :
{
    "email": "pekka@yahoo.com",
    "password": "pekka07",
    "updatedAt": "2024-03-17T17:19:00.898Z"
}

