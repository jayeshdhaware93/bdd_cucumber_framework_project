endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "Issac",
    "job": "ScieSntist"
}

response header date is :
Sun, 17 Mar 2024 08:51:05 GMT

response body is :
{
    "name": "Issac",
    "job": "ScieSntist",
    "id": "60",
    "createdAt": "2024-03-17T08:51:05.632Z"
}

