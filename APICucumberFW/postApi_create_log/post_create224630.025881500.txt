endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "pekka",
    "job": "gamer"
}

response header date is :
Sun, 17 Mar 2024 17:16:30 GMT

response body is :
{
    "name": "pekka",
    "job": "gamer",
    "id": "820",
    "createdAt": "2024-03-17T17:16:30.458Z"
}

