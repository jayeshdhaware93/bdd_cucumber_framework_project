endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "Leo",
    "job": "Artist"
}

response header date is :
Sun, 17 Mar 2024 08:51:04 GMT

response body is :
{
    "name": "Leo",
    "job": "Artist",
    "id": "29",
    "createdAt": "2024-03-17T08:51:04.629Z"
}

