endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "pekka",
    "job": "gamer"
}

response header date is :
Sun, 17 Mar 2024 17:17:53 GMT

response body is :
{
    "name": "pekka",
    "job": "gamer",
    "id": "715",
    "createdAt": "2024-03-17T17:17:53.629Z"
}

