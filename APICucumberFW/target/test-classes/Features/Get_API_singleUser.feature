Feature: trigger GET API to fetch the single user 

@Get_API_Single_user
Scenario: Trigger the request with valid parameters and fech single user's information
        Given configure get_single_user endpoint 
        When trigger the api to hit get_single_user endpoint
        Then validate for status code
        And validate response body parameters of get api
        