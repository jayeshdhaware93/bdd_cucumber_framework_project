Feature: data driven testing 

@Data_driven_testase
Scenario Outline:trigger the post api with valid request data
                Given enter "<Name>" and "<Job>" in request body
                When trigger the api with given request data
                Then validate status code of post api data driven 
                And validate responsebody parameters for data driven
                
Examples:       
                 |Name |Job |
                 |pekka|Gamer|
                 |Leo|Artist|
                 |Issac|ScieSntist|